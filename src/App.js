import React, { Component } from 'react';
import './App.css';
import './components/Display/Display'
import Display from './components/Display/Display';
import Search from './components/Search/Search';
import Utility from './Utility/Utility';
class App extends Component {

  state = ({
    username: "",
    action: "",
    records: [],
    searchUsername: "",
    recordedUsernameList: []
  }
  );

  constructor(props){
    super(props);
    this.Utility = new Utility();
    this.retrieveUsernameList();
  }

  retrieveUsernameList = async() =>{
    try {
      let data = await this.Utility.apiCallGetUsers();
      this.setState({recordedUsernameList: data.data})

    }catch(error){
      console.log("Error detected! Not setting recordedUsernameList.", error);
    }
  }

  render() {
    return (
      <div className="flexcontainer">
        <div className="flexrow">
          <div className="flexcolumn">
            <h1>Audit Log System</h1>
            <h2>It's a fun system!</h2>
          </div>
          <hr/>
          <Display login={this.clickLogin} check={this.clickCheck} username={this.updateUsername}/>
          <hr/>
          <Search click={this.searchClick} search={this.updateSearchParameters} 
                  clear={this.clearResults} records={this.state.records}/>
        </div>
      </div>

    );
  }

  clickLogin = (action) => {
    this.setState( {action: "LOGIN"}, () => {
      this.Utility.apiCallAdd(this.state.username, this.state.action)
      console.log("Triggering request to backend!");
      console.log("Username: %s, Action: %s", this.state.username, this.state.action);
    });
    
  }

  clickCheck = (action) => {
    this.setState({action: "CLICK"}, () => {
      this.Utility.apiCallAdd(this.state.username, this.state.action)
      console.log("Triggering request to backend!");
      console.log("Username: %s, Action: %s", this.state.username, this.state.action);
    });
  }

  searchClick = async () => {
    const response = await this.Utility.apiCallSearch(this.state.searchUsername, null, this.updateRecord);
    console.log(response);
    this.setState({records: response.data}, () => {console.log("Updated Records: ", this.state.records)});
  
  }

  clearResults = () => {
    this.setState({records: []});
  }

  updateUsername = (event) => {
    this.setState({username: event.target.value});
  }

  
  updateSearchParameters = (event) => {
    this.setState({searchUsername: event.target.value})
  }

  updateRecord = (data) => {
    this.setState({records: data.data});
  }

}

export default App;
