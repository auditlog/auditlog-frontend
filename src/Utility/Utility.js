import axios from 'axios';

export default class Utility {

    apiHeader = {'Content-Type': 'application/json'};

    apiCallSearch(username, action, updateState) {
        console.log("Firing Inquiry to backend.", username, action);
        let queryURL = "";
        if(username){
            queryURL = `http://localhost:8080/audit/${username}/inquiry`;
        }
        else{
            queryURL = `http://localhost:8080/audit/inquiry`;
        }

        if(action !== null){
            queryURL = queryURL + `?filter=${action}`;
        }

        console.log("Querying to: ",queryURL);

        return axios.get(queryURL, {headers: this.apiHeader})
            .then(response =>{
                return response;
            })
            .catch(error =>{
                console.log("Error detected!", error);
            })

    }
    
    apiCallAdd(username, action){
        console.log("%s, %s", username, action)
        
        const apiData = {
            action: action,
            username: username
        }
        console.log("Header", this.apiHeader);
        console.log("Json Data: ", JSON.stringify(apiData));
        axios.post("http://localhost:8080/audit/add", JSON.stringify(apiData), {headers: this.apiHeader})
            .then(function(response) {
            console.log("Response: ", response);
            })
            .catch(function(error){
            console.log("Error detected: ", error);
            })
    }

    apiCallGetUsers(){
        console.log("Querying to API for users list")
        return axios.get("http://localhost:8080/ignite/getUsers", {headers: this.apiHeader})
        .then(response => {
            console.log("Response: ", response);
            return response;
        })
        .catch(error => {
            console.log("Error detected: ", error);
        })
    }
}




