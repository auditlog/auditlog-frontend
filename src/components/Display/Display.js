import React from 'react';
import Autosuggest from 'react-autosuggest';

export default (props) => {
  
    return (
        <div className="form-group">
            <label htmlFor="username">Username</label>
            <input type="text" onChange={props.username} className="form-control" id="username" placeholder="Username"/>
            <button onClick={props.login}>Login</button>
            <button onClick={props.check}>Check</button>
        </div>
    );

}
