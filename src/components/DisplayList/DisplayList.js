import React from 'react'
import DisplayBox from '../DisplayBox/DisplayBox';

export default (props) => {
  return props.records.map((record, index) => {
        return <DisplayBox
            id={record.id}
            username={record.username}
            timestamp={record.timestamp}
            action={record.action}
            key={record.id}
        />
  }); 
}
