import React from 'react'
import DisplayList from '../DisplayList/DisplayList'
import './Search.css';

export default (props) => {
  return (
    <div className="flexcontainer">
      <div className="flexrow">
        <div className="form-group">
                <label htmlFor="search">Search For Record</label>
                <input type="text" onChange={props.search} className="form-control" id="search" placeholder="Search Username"/>
                <button className="spacer" onClick={props.click}>Search!</button>
                <button className="spacer" onClick={props.clear}>Clear Results</button>
        </div>
        <div>
        <DisplayList records={props.records}/>
      </div>
      </div>
    </div>
    
  );
}
