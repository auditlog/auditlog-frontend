import React, {Component} from 'react'
import classes from './DisplayBox.module.css';

export default class DisplayBox extends Component{

    componentDidMount(){
        console.log(this.props);
    }

    render() {
        return (
        <div className={classes.box}>
            <p>Audit ID: {this.props.id}</p>
            <p>Username: {this.props.username}</p>
            <p>Timestamp: {this.props.timestamp}</p>
            <p>Action: {this.props.action}</p>
        </div>);
    }

    

}